package kr.ac.jbnu.se.tetris;

import javax.sound.sampled.*;
import java.io.File;


public class BgmSound {
    Clip clip;
    File file;
    AudioInputStream ais;
    public BgmSound(String pathname) {
        try {
            clip = AudioSystem.getClip();
            file = new File(pathname);
            ais = AudioSystem.getAudioInputStream(file);

            clip.open(ais);

            FloatControl gainControl = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
            gainControl.setValue(-15.0f);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void BgmStart() {
            clip.start();
            clip.loop(-1);
    }

    public void BgmStop() {
            clip.stop();
    }
}
