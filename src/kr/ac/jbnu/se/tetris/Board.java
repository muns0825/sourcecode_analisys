package kr.ac.jbnu.se.tetris;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.swing.*;

import java.util.logging.Logger;

//^ㅇ^!!!

public class Board extends JPanel implements ActionListener {

    private final transient FirebaseScoreManager firebaseScoreManager; // FirebaseScoreManager 인스턴스 추가
    private final String modeType; // 모드 타입 추가
    //HEAVYBLOCK
    private int heavyBlockCurrentRow;
    private static final int HEAVY_BLOCK_DELAY = 30;
    private Timer heavyBlockTimer;
    private int heavyBlockCount;

    // 게임 보드의 가로와 세로 크기
    static final int BOARD_WIDTH = 14;
    static final int BOARD_HEIGHT = 28;
    final Timer timer; // 게임 루프용 타이머
    Timer messageTimer;
    String curMessage = "";



    boolean isFallingFinished = false; // 블록이 떨어지는 중인지 여부
    boolean isStarted = false; // 게임이 시작되었는지 여부
    boolean isPaused = false; // 게임이 일시 중지되었는지 여부
    boolean usingSavedPiece = false; // 저장된 블록을 사용사용중인지 여부
    int score = 0; // 현재 점수
    int level = 1; // 현재 레벨
    final int[] levelSpeeds = { 800, 600, 500, 400, 200 }; // 레벨별 블록 내려오는 속도 (밀리초)
    int curX = 0; // 현재 블록의 x 좌표
    int curY = 0; // 현재 블록의 y 좌표
    int ghostY = 0;
    final JLabel statusbar; // 게임 상태 표시 레이블
    transient Shape curPiece; // 현재 블록
    transient Shape ghostPiece;
    final Tetrominoes[] board;// 게임 보드

    transient Shape savedPiece = new Shape(); // 저장된 블록
    private transient NextPiece nextPiece;

    boolean holdable = true; // 블록 저장 여부

    private static final  String TIME_ATTACK = "TimeAttack";
    private int timeLeft; // 타임어택 모드에서 남은 시간
    private final int timeLimit = 120; // 120초(2분) 시간 제한
    boolean feverMode = false;
    int feverCount = 0;
    int lineCount = 0;

    private Color[] defaultColors;
    private Color[] theme1Colors;
    private Color[] theme2Colors;
    private Color[] theme3Colors;
    private Color[] theme4Colors;
    private Color[] selectedSkinColors;
    private transient EffectSound spinSound = new EffectSound("sound/spin.wav");
    private transient EffectSound dropSound = new EffectSound("sound/drop.wav");
    private transient EffectSound removeLIneSound = new EffectSound("sound/removeLine.wav");

    private static final String WELCOME_MESSAGE = "Welcome to Tetris!";
    private static final String ATTACK_MESSAGE = "Attack to Tetris!";
    private final String[] messages = {"G O O D !", "N I C E !", "G R E A T !", "T E T R I S !"};

    private static final String GAME_OVER_TITLE = "게임 오버";
    private static final String RESTART_OPTION = "재시작";
    private static final String EXIT_OPTION = "종료";


    // 게임 보드 생성자
    public Board(Tetris parent, final String modeType, FirebaseScoreManager firebaseScoreManager) {
        String fontFilePath = "C:\\TETRIS\\TTRS\\font\\montano-regular.ttf";

        Font customFont = FontLoader.loadFont(fontFilePath, 16);

        JLabel statusLabel = new JLabel();
        statusLabel.setFont(customFont);

        setFocusable(true);
        curPiece = new Shape();

        nextPiece = new NextPiece();
        heavyBlockCount = 15;

        timer = new Timer(800, this); // 400 밀리초마다 actionPerformed() 호출

        timer.start();

        statusbar = parent.getStatusBar(); // Tetris 클래스에서 상태바 레이블 가져오기
        board = new Tetrominoes[BOARD_WIDTH * BOARD_HEIGHT];
        addKeyListener(new TAdapter()); // 키 이벤트 리스너 등록
        clearBoard();

        this.modeType = modeType; // 멤버 변수 modeType를 인자로 받은 modeType으로 초기화
        this.firebaseScoreManager = firebaseScoreManager; // 멤버 변수 firebaseScoreManager를 인자로 받은 firebaseScoreManager로 초기화

        messageTimer = new Timer(3000, e -> {
            curMessage = "";
            if(modeType.equals("Normal")) {
                statusbar.setText(WELCOME_MESSAGE);
            } else {
                statusbar.setText(ATTACK_MESSAGE);
            }
            messageTimer.stop();
            repaint();
        });

        this.timeLeft = timeLimit;

    }




    // 타이머에서 호출되는 메서드
    public void actionPerformed(ActionEvent e) {
        if (isFallingFinished) {
            isFallingFinished = false;
        } else {
            oneLineDown(); // 블록을 한 칸 아래로 이동
        }
        if (modeType.equals(TIME_ATTACK)) {
            timeLeft--; // 시간 감소
            if (timeLeft <= 0) {
                // 시간 종료, 게임 종료 처리
                timer.stop();
                isStarted = false;
                statusbar.setText("Time's up!");
                gameOver();
            } else {
                // 시간 업데이트
                statusbar.setText(ATTACK_MESSAGE);
            }
        }
    }

    // 게임 보드의 각 정사각형의 가로 크기를 반환
    int squareWidth() {
        return (int) getSize().getWidth() / BOARD_WIDTH;
    }

    // 게임 보드의 각 정사각형의 세로 크기를 반환
    int squareHeight() {
        return (int) getSize().getHeight() / BOARD_HEIGHT;
    }

    // 지정된 위치의 Tetrominoes를 반환
    Tetrominoes shapeAt(int x, int y) {
        return board[(y * BOARD_WIDTH) + x];
    }

    public void boardInit(String modeType){
        isStarted = true;
        isFallingFinished = false;
        isPaused = false;
        usingSavedPiece = false;
        score = 0;
        level = 1;
        curX = 0;
        curY = 0;
        ghostY = 0;
        holdable = true;
        savedPiece = new Shape();
        curPiece = new Shape();
        ghostPiece = new Shape();
        feverMode = false;
        clearBoard();

        // 상태바 초기화
        if(modeType.equals("Normal")) {
            statusbar.setText(WELCOME_MESSAGE);
        }
        else{
            statusbar.setText(ATTACK_MESSAGE);
        }

        // 타이머 재시작
        timer.start();

        newPiece(); // 새로운 블록 생성
        repaint(); // 게임 화면 다시 그리기
    }

    // 게임을 시작하는 메서드
    public void start() {
        if (isPaused)
            return;

        isStarted = true;
        isFallingFinished = false;
        clearBoard();

        newPiece(); // 새로운 블록 생성
        timer.start(); // 타이머 시작
        if (modeType.equals(TIME_ATTACK)) {
            statusbar.setText(ATTACK_MESSAGE);
        } else {
            statusbar.setText(WELCOME_MESSAGE);
        }
    }


    // 게임 재시작 메서드
    public void restartGame() {
    boardInit(modeType);
        if (modeType.equals(TIME_ATTACK)) {
            timeLeft = timeLimit;
            statusbar.setText(ATTACK_MESSAGE);
        }
    }

    // 게임 일시 중지 메서드
    private void pause() {
        if (!isStarted)
            return;

        isPaused = !isPaused;
        if (isPaused) {
            timer.stop();
            statusbar.setText("Paused"); // 상태바에 "Paused" 표시
        } else {
            timer.start();
            heavyBlockTimer.start();
            statusbar.setText("Game has started again");
            messageTimer.start();
        }
        repaint();
    }

    @Override
    // 게임 화면 그리기
    public void paint(Graphics g) {
        super.paint(g);

        Dimension size = getSize();
        int boardTop = calculateBoardTop(size);
        // 사용자가 다운로드한 폰트 파일 경로로 수정
        String fontFilePath = "C:\\TETRIS\\TTRS\\font\\Mabinogi_Classic_TTF.ttf";

        // 폰트 파일을 사용하여 폰트 객체 생성
        Font customFont = FontLoader.loadFont(fontFilePath, 16);

        // 폰트를 적용할 레이블 생성
        JLabel statusLabel = new JLabel();

        // 레이블에 폰트 설정
        statusLabel.setFont(customFont);


        drawBackground(g, size);
        drawGrid(g, boardTop);
        drawHoldBox(g, boardTop);
        drawNextBox(g, boardTop);
        drawGameBoard(g, boardTop);
        drawCurrentPiece(g, boardTop);
        drawScoreAndLevel(g);
        drawRemovalMessage(g);
        drawHeavyBlockCounter(g);

    }

    private void drawGrid(Graphics g, int boardTop) {
        g.setColor(Color.LIGHT_GRAY);
        for (int i = 0; i <= BOARD_WIDTH; ++i) {
            g.drawLine(i * squareWidth(), boardTop, i * squareWidth(), boardTop + BOARD_HEIGHT * squareHeight());
        }

        for (int j = 0; j <= BOARD_HEIGHT; ++j) {
            g.drawLine(0, boardTop + j * squareHeight(), BOARD_WIDTH * squareWidth(), boardTop + j * squareHeight());
        }
    }

    private int calculateBoardTop(Dimension size) {
        return (int) size.getHeight() - BOARD_HEIGHT * squareHeight();
    }

    private void drawHoldBox(Graphics g, int boardTop) {
        // 사용자가 다운로드한 폰트 파일 경로로 수정
        String fontFilePath = "C:\\TETRIS\\TTRS\\font\\Mabinogi_Classic_TTF.ttf";

        // 폰트 파일을 사용하여 폰트 객체 생성
        Font customFont = FontLoader.loadFont(fontFilePath, 16);

        // 폰트를 적용할 레이블 생성
        JLabel statusLabel = new JLabel();

        // 레이블에 폰트 설정
        statusLabel.setFont(customFont);
        int holdBoxLeft = 260;
        int holdBoxWidth = 110;
        int holdBoxHeight = 110;
        // 홀드 박스에 항상 "홀드 박스입니다"라는 텍스트 표시
        String holdText = " Hold Piece";

        // 텍스트를 가운데 정렬하여 그리기
        g.setColor(Color.BLACK);
        g.setFont(customFont);
        FontMetrics fontMetrics = g.getFontMetrics();
        int textWidth = fontMetrics.stringWidth(holdText);
        int x = holdBoxLeft + (holdBoxWidth - textWidth) / 2;
        int y = boardTop + holdBoxHeight + 15;
        g.drawString(holdText, x, y);

        g.setColor(Color.BLACK);
        g.drawRect(holdBoxLeft, boardTop, holdBoxWidth, holdBoxHeight);

        if (!holdable) {
            drawHoldPiece(g, holdBoxLeft, boardTop, holdBoxWidth, holdBoxHeight);
        }
    }

    private void drawNextBox(Graphics g, int boardTop) {
        String fontFilePath = "C:\\TETRIS\\TTRS\\font\\Mabinogi_Classic_TTF.ttf";
        // 폰트 파일을 사용하여 폰트 객체 생성
        Font customFont = FontLoader.loadFont(fontFilePath, 16);
        // 폰트를 적용할 레이블 생성
        JLabel statusLabel = new JLabel();

        int nextBoxLeft = 260;
        int nextBoxWidth = 110;
        int nextBoxHeight = 110;
        boardTop = boardTop + 130;

        String nextText = " Next Piece";

        // 텍스트를 가운데 정렬하여 그리기
        g.setColor(Color.BLACK);
        g.setFont(customFont);
        FontMetrics fontMetrics = g.getFontMetrics();
        int textWidth = fontMetrics.stringWidth(nextText);
        int x = nextBoxLeft + (nextBoxWidth - textWidth) / 2;
        int y = boardTop + nextBoxHeight + 15;
        g.drawString(nextText, x, y);

        g.setColor(Color.BLACK);
        g.drawRect(nextBoxLeft, boardTop, nextBoxWidth, nextBoxHeight);

        g.setColor(Color.BLACK);
        g.drawRect(nextBoxLeft, boardTop, nextBoxWidth, nextBoxHeight);

        if (curPiece.getTshape() != Tetrominoes.NO_SHAPE) {
            drawNextPiece(g, nextBoxLeft, boardTop, nextBoxWidth, nextBoxHeight);
        }
    }

    private void drawNextPiece(Graphics g, int nextBoxLeft, int boardTop, int nextBoxWidth, int nextBoxHeight) {
        Tetrominoes nextShape = nextPiece.getNextPiece();
        if (Tetrominoes.NO_SHAPE != nextShape) {
            for (int i = 0; i < 4; ++i) {
                int x = nextPiece.x(i);
                int y = nextPiece.y(i);

                int drawX = nextBoxLeft + nextBoxWidth / 2 + (x-1) * squareWidth();
                int drawY = boardTop - nextBoxHeight / 2 + (y+4) * squareHeight();
                drawSquare(g, drawX, drawY, nextShape, false);
            }
        }
    }

    private void drawGameBoard(Graphics g, int boardTop) {
        for (int i = 0; i < BOARD_HEIGHT; ++i) {
            for (int j = 0; j < BOARD_WIDTH; ++j) {
                Tetrominoes shape = shapeAt(j, BOARD_HEIGHT - i - 1);
                if (shape != Tetrominoes.NO_SHAPE)
                    drawSquare(g, j * squareWidth(), boardTop + i * squareHeight(), shape, false);
            }
        }
    }

    private void drawCurrentPiece(Graphics g, int boardTop) {
        if (curPiece.getTshape() != Tetrominoes.NO_SHAPE) {
            for (int i = 0; i < 4; ++i) {
                int x = curX + curPiece.x(i);
                int y = curY - curPiece.y(i);
                drawSquare(g, x * squareWidth(), boardTop + (BOARD_HEIGHT - y - 1) * squareHeight(), curPiece.getTshape(), false);
            }
            drawGhostPiece(g, boardTop);
            drawTimeAttackMode(g);
            drawHeavyBlockCounter(g);
        }
    }

    private void drawBackground(Graphics g, Dimension size) {
        if (!isStarted) {
            g.setColor(new Color(255, 255, 255, 255));
        } else {
            g.setColor(new Color(255, 255, 255, 255));
        }
        g.fillRect(0, 0, size.width, size.height);
    }

    private void drawScoreAndLevel(Graphics g) {
        g.setColor(Color.BLACK);
        g.drawString("Score: " + score, 10, 20);
        g.drawString("Level: " + level, 10, 40);
    }

    private void drawRemovalMessage(Graphics g) {
        if (!curMessage.isEmpty()) {
            g.setColor(Color.BLACK);

            // 기본 폰트 사용
            g.setFont(new Font("SansSerif", Font.PLAIN, 20));

            FontMetrics fm = g.getFontMetrics();
            int stringWidth = fm.stringWidth(curMessage);
            int x = (400 - stringWidth) / 2;
            g.drawString(curMessage, x, 20);
        }
    }

    private void drawHoldPiece(Graphics g, int holdBoxLeft, int boardTop, int holdBoxWidth, int holdBoxHeight) {
        Tetrominoes shape = savedPiece.getTshape();
        if (shape != Tetrominoes.NO_SHAPE) {
            for (int i = 0; i < 4; ++i) {
                int x = savedPiece.x(i);
                int y = savedPiece.y(i);
                int drawX = holdBoxLeft + holdBoxWidth / 2 + (x - 1) * squareWidth();
                int drawY = boardTop - holdBoxHeight / 2 + (y+4) * squareHeight();
                drawSquare(g, drawX, drawY, shape, false);
            }
        }
    }



    private void drawGhostPiece(Graphics g, int boardTop) {
        for (int j = 0; j < 4; ++j) {
            ghostPiece();
            int x = curX + ghostPiece.x(j);
            int y = ghostY - ghostPiece.y(j);
            drawSquare(g, x * squareWidth(), boardTop + (BOARD_HEIGHT - y - 1) * squareHeight(), ghostPiece.getTshape(), true);
        }
    }

    private void drawTimeAttackMode(Graphics g) {
        if (modeType.equals(TIME_ATTACK)) {
            g.drawString("Time: " + timeLeft, 10, 60);
        }
    }

    private void drawHeavyBlockCounter(Graphics g) {
        if(modeType.equals(TIME_ATTACK)){
            g.drawString("HeavyBlock: " + heavyBlockCount, 10, 75);
        }else {
            g.drawString("HeavyBlock: " + heavyBlockCount, 10, 60);
        }
    }


    private void ghostPiece() {
        int newY = curY;
        while (newY > 0) {
            if (!tryMove(curPiece, curX, newY - 1, true))
                break;
            --newY;
        }
    }

    // 현재 블록을 아래로 떨어뜨리는 메서드
    private void dropDown() {
        int newY = curY;
        while (newY > 0) {
            if (!tryMove(curPiece, curX, newY - 1, false))
                break;
            --newY;
        }
        pieceDropped(); // 블록을 떨어뜨린 후의 처리
    }

    // 현재 블록을 한 칸 아래로 이동하는 메서드
    private void oneLineDown() {
        if (!tryMove(curPiece, curX, curY - 1, false))
            pieceDropped(); // 블록을 떨어뜨린 후의 처리
    }

    // 게임 보드 초기화
    private void clearBoard() {
        for (int i = 0; i < BOARD_HEIGHT * BOARD_WIDTH; ++i)
            board[i] = Tetrominoes.NO_SHAPE;
    }

    // 블록이 떨어진 후의 처리
    private void pieceDropped() {
        for (int i = 0; i < 4; ++i) {
            int x = curX + curPiece.x(i);
            int y = curY - curPiece.y(i);
            board[(y * BOARD_WIDTH) + x] = curPiece.getTshape();
        }
        if(curPiece.getTshape() != Tetrominoes.HEAVY_SHAPE) {
            removeFullLines();
        }else if (curPiece.getTshape() == Tetrominoes.HEAVY_SHAPE){
            HeavyBlockDown();
            removeFullLines();
        }

        if (!isFallingFinished)
            dropSound.EffectStart();
            newPiece(); // 새로운 블록 생성
    }

    // 새로운 블록 생성
    private void newPiece() {
        Tetrominoes nextShape = nextPiece.getNextPiece();
        if(!isFeverMode()) {
            curPiece.setShape(nextShape);
        } else {
            if(feverCount < 5) {
                curPiece.setShape(Tetrominoes.ONE_SHAPE);
                feverCount++;
            } else {
                curPiece.setShape(nextShape);
                feverMode = false;
                feverCount = 0;
            }
        }
        curX = BOARD_WIDTH / 2 + 1;
        curY = BOARD_HEIGHT - 1 + curPiece.minY();

        usingSavedPiece = false;

        if (!tryMove(curPiece, curX, curY, false)) {
            curPiece.setShape(Tetrominoes.NO_SHAPE);
            ghostPiece.setShape(Tetrominoes.NO_SHAPE);
            timer.stop();
            isStarted = false;
            statusbar.setText("Game Over"); // 상태바에 "Game Over" 표시
            repaint();

            gameOver();
        }
        nextPiece.generateNextPiece();
    }


    public void gameOver() {
        firebaseScoreManager.updateHighScore(modeType, score);

        Map<String, Object> scoreData = firebaseScoreManager.getTopScoresAndRank(modeType);

        String message = "게임 오버!\n현재 점수: " + score;
        Object scoreValue = scoreData.get("userScore");

        if (scoreValue != null) {
            message += "\n나의 최고 점수: " + scoreValue;
        }

        StringBuilder topScoresMessage = new StringBuilder("상위 3명의 유저 아이디와 점수:\n");
        List<Map<String, Object>> topScores = (List<Map<String, Object>>) scoreData.get("topScores");
        Collections.reverse(topScores);

        for (int i = 0; i < Math.min(topScores.size(), 3); i++) {
            Map<String, Object> userScore = topScores.get(i);
            String userID = (String) userScore.get("userID");
            Object userScoreValue = userScore.get("score");

            if (userID != null && userScoreValue != null) {
                topScoresMessage.append(i + 1).append(". ").append(userID).append(" : ").append(userScoreValue).append("점\n");
            }
        }

        showGameOverDialog(message + "\n\n" + topScoresMessage.toString(), "게임 재시작 또는 종료하시겠습니까?");
    }

    private void showGameOverDialog(String message, String optionMessage) {
        JOptionPane.showMessageDialog(
                null,
                message,
                GAME_OVER_TITLE,
                JOptionPane.INFORMATION_MESSAGE
        );

        int option = JOptionPane.showOptionDialog(
                null,
                optionMessage,
                GAME_OVER_TITLE,
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,
                new String[]{RESTART_OPTION, EXIT_OPTION},
                RESTART_OPTION
        );

        if (option == JOptionPane.YES_OPTION) {
            restartGame();
        } else {
            System.exit(0);
        }
    }





    // 블록을 이동하려는 위치로 이동할 수 있는지 확인하는 메서드
    public boolean tryMove(Shape newPiece, int newX, int newY, boolean ghost) {
        for (int i = 0; i < 4; ++i) {
            int x = newX + newPiece.x(i);
            int y = newY - newPiece.y(i);
            if (x < 0 || x >= BOARD_WIDTH || y < 0 || y >= BOARD_HEIGHT)
                return false;
            if (shapeAt(x, y) != Tetrominoes.NO_SHAPE)
                return false;
        }

        if (!ghost) {
            curPiece = newPiece; // 이동시킬 미노를 현재 미노에 저장
            curX = newX; // 이동시킬 x 좌표를 현재 x 좌표에 저장
            curY = newY; // 이동시킬 y 좌표를 현재 y 좌표에 저장
        } else {
            ghostPiece = newPiece;
            ghostY = newY;
        }
        repaint(); // 화면 재출력
        return true; // if문이 작동되지 않으면 true 값을 반환하여 현재 값에 이동시킬 값들을 저장 후 블록과 좌표를 이동
    }


    private boolean isLineFull(int lineIndex) {
        for (int j = 0; j < BOARD_WIDTH; ++j) {
            if (shapeAt(j, lineIndex) == Tetrominoes.NO_SHAPE) {
                return false;
            }
        }
        return true;
    }
    // 가득 찬 줄을 제거하는 메서드
    private void removeFullLines() {
        int numFullLines = countFullLines();

        if (numFullLines > 0) {
            updateGameAfterRemovingLines(numFullLines);
        }
    }

    private int countFullLines() {
        int numFullLines = 0;

        for (int i = BOARD_HEIGHT - 1; i >= 0; --i) {
            if (isLineFull(i)) {
                removeLineAt(i);
                ++numFullLines;
            }
        }

        return numFullLines;
    }

    private void removeLineAt(int lineIndex) {
        removeLIneSound.EffectStart();
        ++lineCount;
        // 1줄을 없앨 때마다 시간을 5초 추가
        timeLeft += 5;

        for (int k = lineIndex; k < BOARD_HEIGHT - 1; ++k) {
            for (int j = 0; j < BOARD_WIDTH; ++j) {
                board[(k * BOARD_WIDTH) + j] = shapeAt(j, k + 1);
            }
        }
    }

    private void updateGameAfterRemovingLines(int numFullLines) {
        updateScoreAndLevel(numFullLines);
        displayMessage(numFullLines);
        adjustFallingSpeed();

        clearFullLines();
        repaint();
        resetPiecesIfFinished();
    }

    private void updateScoreAndLevel(int numFullLines) {
        int lineScore = 100 * numFullLines;
        level = score / 1000 + 1; // 1000점 단위로 레벨 업
        score += level * lineScore;
    }

    private void displayMessage(int numFullLines) {
        if (numFullLines >= 1 && numFullLines <= 4) {
            curMessage = messages[numFullLines - 1];
            statusbar.setText(curMessage);
            messageTimer.start();
        }
    }


    private void adjustFallingSpeed() {
        int levelSpeedIndex = Math.min(level, levelSpeeds.length - 1);
        int newDelay = levelSpeeds[levelSpeedIndex];
        timer.setDelay(newDelay);
    }

    private void clearFullLines() {
        int dest = BOARD_HEIGHT - 1;

        for (int src = BOARD_HEIGHT - 1; src >= 0; --src) {
            if (!isLineFull(src)) {
                moveNonFullLineToDestination(src, dest);
            }
            dest--;
        }

        setRemainingTopRowsToEmpty(dest);
    }

    private void moveNonFullLineToDestination(int src, int dest) {
        for (int k = 0; k < BOARD_WIDTH; ++k) {
            board[(dest * BOARD_WIDTH) + k] = shapeAt(k, src);
        }
    }

    private void setRemainingTopRowsToEmpty(int dest) {
        for (int i = dest; i >= 0; --i) {
            for (int k = 0; k < BOARD_WIDTH; ++k) {
                board[(i * BOARD_WIDTH) + k] = Tetrominoes.NO_SHAPE;
            }
        }
    }

    private void resetPiecesIfFinished() {
        if (isFallingFinished) {
            isFallingFinished = false;
            ghostPiece.setShape(Tetrominoes.NO_SHAPE);
        }
    }





    private boolean isFeverMode(){
       if(lineCount >= 10) {
           lineCount = 0;
           feverMode = true;
           curMessage = "F E V E R !";
       }
        return feverMode;
    }
private void initializeThemes(){
    defaultColors = new Color[] {
            new Color(0, 0, 0),
            new Color(204, 102, 102),
            new Color(102, 204, 102),
            new Color(102, 102, 204),
            new Color(204, 204, 102),
            new Color(204, 102, 204),
            new Color(102, 204, 204),
            new Color(218, 170, 0),
            new Color(72, 143, 211),
            new Color(92, 49, 30)
    };

    theme1Colors = new Color[] {
            new Color(0, 0, 0),
            new Color(237, 210, 224),
            new Color(237, 187, 180),
            new Color(219, 171, 190),
            new Color(186, 161, 167),
            new Color(226, 228, 219),
            new Color(218, 221, 182),
            new Color(201, 204, 184),
            new Color(121, 123, 132),
            new Color(92, 49, 30)
    };

    theme2Colors = new Color[] {
            new Color(0, 0, 0),
            new Color(9, 12, 155),
            new Color(61, 82, 213),
            new Color(180, 197, 228),
            new Color(251, 255, 241),
            new Color(9, 93, 87),
            new Color(62, 156, 150),
            new Color(180, 214, 213),
            new Color(59, 62, 61),
            new Color(92, 49, 30)
    };

    theme3Colors = new Color[] {
            new Color(0, 0, 0),
            new Color(63, 13, 18),
            new Color(167, 29, 49),
            new Color(241, 240, 204),
            new Color(213, 191, 134),
            new Color(141, 119, 95),
            new Color(115, 125, 43),
            new Color(204, 206, 151),
            new Color(132, 134, 102),
            new Color(92, 49, 30)
    };

    theme4Colors = new Color[] {
            new Color(0, 0, 0),
            new Color(147, 183, 190),
            new Color(85, 67, 72),
            new Color(212, 245, 245),
            new Color(140, 154, 158),
            new Color(116, 117, 120),
            new Color(72, 72, 72),
            new Color(224, 221, 245),
            new Color(224, 239, 239),
            new Color(92, 49, 30)
    };
}


    private void selectSkinColors() {
        switch (SettingMenu.curSkin) {
            case SettingMenu.SKIN_2 -> selectedSkinColors = theme1Colors;
            case SettingMenu.SKIN_3 -> selectedSkinColors = theme2Colors;
            case SettingMenu.SKIN_4 -> selectedSkinColors = theme3Colors;
            case SettingMenu.SKIN_5 -> selectedSkinColors = theme4Colors;
            default -> selectedSkinColors = defaultColors;
        }
    }
    // 정사각형을 그리는 메서드
    private void drawSquare(Graphics g, int x, int y, Tetrominoes shape, boolean ghost) {
        initializeThemes();
        selectSkinColors();

        Color color = selectedSkinColors[shape.ordinal()];

        if (!ghost) {
            g.setColor(color);
            g.fillRect(x + 1, y + 1, squareWidth() - 2, squareHeight() - 2);
        }

        g.setColor(Color.BLACK);
        g.drawLine(x, y + squareHeight() - 1, x, y);
        g.drawLine(x, y, x + squareWidth() - 1, y);

        g.setColor(Color.BLACK);
        g.drawLine(x + 1, y + squareHeight() - 1, x + squareWidth() - 1, y + squareHeight() - 1);
        g.drawLine(x + squareWidth() - 1, y + squareHeight() - 1, x + squareWidth() - 1, y + 1);
    }


    // 블록 홀드 메서드
    private void holdPiece() {
        if (holdable) {
            savedPiece.setShape(curPiece.getTshape());
            holdable = false;
            statusbar.setText("블록이 저장 되었습니다!");
            statusbar.setText("저장된 블록: " + savedPiece.getTshape());
            newPiece();
        }else{
            statusbar.setText("이미 저장된 블록이 있습니다!");
        }
        repaint();
    }

    //저장된 블록을 다시 사용 메서드
    private void restoreSavedPiece(){
        if (!holdable && !usingSavedPiece) {
            curPiece = new Shape();
            curPiece.setShape(savedPiece.getTshape());
            curX = BOARD_WIDTH / 2 + 1;
            curY = BOARD_HEIGHT - 1 + curPiece.minY();
            statusbar.setText("저장된 블록을 사용했습니다.");
            repaint();
            usingSavedPiece = true;
            holdable = true;

        } else if (usingSavedPiece) {
            statusbar.setText("저장된 블록을 사용중입니다.");
        } else {
            statusbar.setText("저장된 블록이 없습니다.");
        }
    }

    //HEAVY블럭
    private void storedHeavyBlock() {

        if (heavyBlockCount > 0 ) {

            curPiece.setShape(Tetrominoes.HEAVY_SHAPE);
            heavyBlockCount--;

            curX = BOARD_WIDTH / 2;
            curY = BOARD_HEIGHT - 1 - curPiece.minY();
            System.out.println("call heavyBlock");

        }
        if (heavyBlockCount < 0 ){
            statusbar.setText("heavy block 없음");
        }
    }

    private void HeavyBlockDown() {
        heavyBlockCurrentRow = BOARD_HEIGHT - 1;
        heavyBlockTimer = new Timer(HEAVY_BLOCK_DELAY, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (heavyBlockCurrentRow > 0) {
                    heavyBlockDownStep(heavyBlockCurrentRow);
                    heavyBlockCurrentRow--;
                } else {
                    ((Timer) e.getSource()).stop();
                    isFallingFinished = true;
                }
            }
        });
        heavyBlockTimer.start();
        timer.start();

    }

    private void heavyBlockDownStep(int currentRow) {
        System.out.println("2");
        for (int j = 0; j < BOARD_WIDTH; ++j) {
            System.out.println("2. for문");
            if (shapeAt(j, currentRow) == Tetrominoes.HEAVY_SHAPE) {
                // 현재 위치의 헤비 블록을 제거합니다.
                board[(currentRow * BOARD_WIDTH) + j] = Tetrominoes.NO_SHAPE;
                repaint();

                // 헤비 블록을 아래로 이동합니다.
                int newRow = currentRow - 1;
                if (newRow >= 0) {
                    board[(newRow * BOARD_WIDTH) + j] = Tetrominoes.HEAVY_SHAPE;
                }
            }
        }
        repaint();
    }



    // 키 이벤트 리스너 클래스
    class TAdapter extends KeyAdapter {

        @Override
        public void keyPressed(KeyEvent e) {

            if (!isStarted || curPiece.getTshape() == Tetrominoes.NO_SHAPE) {
                return;
            }

            int keycode = e.getKeyCode();

            if (keycode == 'p' || keycode == 'P') {
                pause(); // 게임 일시 중지/재개
                return;
            }

            if (isPaused)
                return;

            switch (keycode) {
                case KeyEvent.VK_LEFT:
                    tryMove(curPiece, curX - 1, curY, false); // 왼쪽으로 이동
                    break;
                case KeyEvent.VK_RIGHT:
                    tryMove(curPiece, curX + 1, curY, false); // 오른쪽으로 이동
                    break;
                case KeyEvent.VK_DOWN:
                    tryMove(curPiece.rotateRight(), curX, curY, false); // 시계 방향으로 회전
                    spinSound.EffectStart();
                    break;
                case KeyEvent.VK_UP:
                    tryMove(curPiece.rotateLeft(), curX, curY, false); // 반시계 방향으로 회전
                    spinSound.EffectStart();
                    break;
                case KeyEvent.VK_SPACE:
                    dropDown(); // 블록을 떨어뜨림
                    dropSound.EffectStart();
                    break;
                case 'd', 'D':
                    oneLineDown(); // 한 칸 아래로 이동
                    break;
                case 'r', 'R':
                    if (!usingSavedPiece) {
                        holdPiece(); //현재 블록을 저장
                    }
                    break;
                case 't', 'T':
                    restoreSavedPiece(); //저장된 블록을 사용
                    break;
                case '1' :
                    storedHeavyBlock();
                    break;
                default:
                    // 예상치 못한 키에 대한 처리
                    break;
            }

        }
    }

}
