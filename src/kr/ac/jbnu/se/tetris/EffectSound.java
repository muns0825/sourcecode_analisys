package kr.ac.jbnu.se.tetris;

import javax.sound.sampled.*;
import java.io.File;

public class EffectSound {
    private Clip clip;

    public EffectSound(String pathname) {
        try {
            AudioInputStream ais = AudioSystem.getAudioInputStream(new File(pathname));
            clip = AudioSystem.getClip();
            clip.open(ais);

            if(pathname.equals("sound/spin.wav")) {
                FloatControl gainControl = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
                gainControl.setValue(-15.0f);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void EffectStart() {
        if (clip.isRunning())
            clip.stop();  // 이미 재생 중인 경우 중지
        clip.setFramePosition(0);
        clip.start(); // 오디오 재생
    }
}