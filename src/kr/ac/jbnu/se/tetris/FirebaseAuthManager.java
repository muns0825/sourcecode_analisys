package kr.ac.jbnu.se.tetris;

import com.google.firebase.database.*;
import java.util.logging.Logger;

public class FirebaseAuthManager {
    private static final Logger logger = Logger.getLogger(FirebaseAuthManager.class.getName());
    public static DatabaseReference databaseReference;
    private static String loggedInUserID;


    public interface SignInCallback {
        void onSignInSuccess(boolean success);
    }

    public interface SignUpCallback {
        void onSignUpSuccess(boolean success);
    }

    // Constructor
    public FirebaseAuthManager() {
        loggedInUserID = null; // 초기화
        initDatabaseReference();
    }
    public static void initDatabaseReference() {
        FirebaseManger.init();
        databaseReference = FirebaseManger.getDatabaseReference();
    }

    // 로그인한 사용자 ID를 반환하는 메서드
    public static String getLoggedInUserID() {
        return loggedInUserID;
    }

    public void signUp(String userID, final String password, final SignUpCallback callback) {
        // 사용자명 또는 비밀번호가 비어 있으면 회원가입 실패
        if (userID.isEmpty() || password.isEmpty()) {
            logger.warning("사용자명 또는 비밀번호가 비어 있습니다.");
            return;
        }
        initDatabaseReference();
        // Firebase Realtime Database에서 사용자 정보를 확인
        final DatabaseReference userRef = databaseReference.child("users").child(userID);

        // 이미 사용자가 존재하면 회원가입 실패
        userRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    // 이미 사용자가 존재하면 회원가입 실패
                    logger.warning("이미 존재하는 사용자입니다.");
                    callback.onSignUpSuccess(false);
                } else {
                    // 사용자 정보 저장
                    userRef.child("password").setValue(password, (databaseError, databaseReference) -> {
                        if (databaseError != null) {
                            // 저장에 실패한 경우
                            logger.severe("데이터 저장 실패");
                            callback.onSignUpSuccess(false);
                        } else {
                            // 저장에 성공한 경우
                            logger.info("User signed up successfully!");
                            callback.onSignUpSuccess(true);
                        }
                    });
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // 읽기 작업이 취소된 경우
                logger.warning("데이터베이스 읽기 취소: " + databaseError.getMessage());
            }
        });
    }

    public static void signIn(final String userID, final String password, final SignInCallback callback) {
        initDatabaseReference();
        // Firebase Realtime Database에서 사용자 정보 검색
        DatabaseReference userRef = databaseReference.child("users").child(userID);

        userRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    String storedPassword = dataSnapshot.child("password").getValue(String.class);
                    if (password.equals(storedPassword)) {
                        // 입력한 비밀번호와 저장된 비밀번호가 일치하는 경우
                        // 로그인 성공
                        loggedInUserID = userID; // 로그인 성공 시 사용자 ID 설정
                        logger.info("로그인 성공");
                        callback.onSignInSuccess(true);
                    } else {
                        // 비밀번호가 일치하지 않는 경우
                        // 로그인 실패
                        logger.warning("로그인 실패");
                        callback.onSignInSuccess(false);
                    }
                } else {
                    // 사용자가 존재하지 않는 경우
                    logger.warning("사용자가 존재하지 않음");
                    callback.onSignInSuccess(false);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // 데이터 검색 중 오류가 발생한 경우 처리
                logger.warning("데이터베이스 검색 중 오류 발생: " + databaseError.getMessage());
                callback.onSignInSuccess(false);
            }
        });
    }
}
