package kr.ac.jbnu.se.tetris;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.FileInputStream;
import java.io.IOException;

public class FirebaseManger {
    private static boolean isInitialized = false; //파이어 베이스 초기화 여부 중복 초기화 방지
    private static DatabaseReference databaseReference;

    public static DatabaseReference getDatabaseReference() {
        return databaseReference;
    }

    public static void init() {
        if (!isInitialized) {
            try {
                // Firebase 초기화 설정
                FileInputStream serviceAccount = new FileInputStream("tetris-b3a39-firebase-adminsdk-j9ug9-2085dce4d5.json");

                FirebaseOptions options = new FirebaseOptions.Builder()
                        .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                        .setDatabaseUrl("https://tetris-b3a39-default-rtdb.firebaseio.com")
                        .build();

                FirebaseApp.initializeApp(options);
                FirebaseDatabase database = FirebaseDatabase.getInstance();
                databaseReference = database.getReference(); // 루트 레퍼런스를 얻습니다.

                isInitialized = true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
