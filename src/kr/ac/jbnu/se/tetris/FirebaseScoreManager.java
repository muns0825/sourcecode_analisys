package kr.ac.jbnu.se.tetris;

import com.google.firebase.database.*;

import java.util.*;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicReference;
import java.io.Serializable;


public class FirebaseScoreManager {
    static String target_userID;
    static DatabaseReference highScoresRef; // 최고 점수를 저장할 레퍼런스

    public FirebaseScoreManager(String userID) {
        target_userID = userID;
        highScoresRef = FirebaseManger.getDatabaseReference().child("highScores");
    }



    // 최고 점수를 업데이트하는 메서드
    public void updateHighScore(String modeType, int newScore) {
        DatabaseReference updateScoreRef = highScoresRef.child(modeType).child(target_userID);
        updateScoreRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Integer currentHighScore = dataSnapshot.getValue(Integer.class);

                if (currentHighScore == null || newScore > currentHighScore) {
                    // 최고 점수가 없거나 현재 점수보다 높으면 업데이트
                    updateScoreRef.setValue(newScore, (databaseError, databaseReference) -> {
                        if (databaseError != null) {
                            // 업데이트에 실패한 경우
                            System.out.println("데이터 업데이트 실패");
                        } else {
                            // 업데이트에 성공한 경우
                            System.out.println("데이터 업데이트 성공");
                        }
                    });
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("데이터베이스 읽기 취소: " + databaseError.getMessage());
            }
        });
    }

    public Map<String, Object> getTopScoresAndRank(String modeType) {
        DatabaseReference scoresRef = highScoresRef.child(modeType);

        // 모드 타입에 대한 top3 점수를 가져옴
        DataSnapshot dataSnapshot = getSnapshotBlocking(scoresRef.orderByValue().limitToLast(3));

        List<Map<String, Object>> topScores = new ArrayList<>();

        Map<String, Integer> userRanks = new HashMap<>(); // 유저별 순위를 저장하는 맵

        int rank = 1;
        Object userScoreValue = null; // 현재 유저의 점수를 저장할 변수

        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
            String userID = snapshot.getKey();
            Object scoreValue = snapshot.getValue();

            // 필요한 정보를 직접 맵에 담음.
            Map<String, Object> userScore = new HashMap<>();
            userScore.put("userID", userID);
            userScore.put("score", scoreValue);
            topScores.add(userScore);

            // 현재 유저의 점수 정보 추가

            // 유저별 순위를 저장
            userRanks.put(userID, rank);

            rank++;
        }

        Map<String, Object> data = new HashMap<>();
        data.put("modeType", modeType);
        data.put("topScores", topScores);
        System.out.println(data);
        return data;
    }



    private static DataSnapshot getSnapshotBlocking(Query query) {
        final Semaphore semaphore = new Semaphore(0);
        final AtomicReference<DataSnapshot> dataSnapshot = new AtomicReference<>();

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                dataSnapshot.set(snapshot);
                semaphore.release();
            }

            @Override
            public void onCancelled(DatabaseError error) {
                semaphore.release();
            }
        });

        try {
            semaphore.acquire();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }

        return dataSnapshot.get();
    }

}
