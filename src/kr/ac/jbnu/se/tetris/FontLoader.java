package kr.ac.jbnu.se.tetris;

import java.awt.Font;
import java.io.File;

public class FontLoader {
    public static Font loadFont(String fontFilePath, float fontSize) {

        try {
            File fontFile = new File(fontFilePath);
            return Font.createFont(Font.TRUETYPE_FONT, fontFile).deriveFont(Font.BOLD,fontSize);
        } catch (Exception e) {
            e.printStackTrace();
            // 예외 처리: 폰트 로딩 실패
            return null;
        }
    }
}