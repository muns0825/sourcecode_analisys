package kr.ac.jbnu.se.tetris;

import javax.swing.*;

public class HoldBox extends JPanel {

    final int HoldBoxWidth = 6;
    final int HoldBoxHeight = 6;


    final Tetrominoes[] holdBox;

    public HoldBox() {
        holdBox = new Tetrominoes[HoldBoxWidth * HoldBoxHeight];
        clearHoldBox();
    }


    void clearHoldBox() {
        for (int i = 0; i < HoldBoxHeight * HoldBoxWidth; ++i)
            holdBox[i] = Tetrominoes.NO_SHAPE;
    }


}
