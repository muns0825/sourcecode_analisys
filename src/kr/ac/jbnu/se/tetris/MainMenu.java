package kr.ac.jbnu.se.tetris;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;

public class MainMenu extends JFrame {
    public MainMenu() {

        try {
            String fontPath = "C:\\TETRIS\\TTRS\\font\\Mabinogi_Classic_TTF.ttf";
            File fontFile = new File(fontPath);

            // 예외 처리 추가
            try {
                Font customFont = Font.createFont(Font.TRUETYPE_FONT, fontFile).deriveFont(Font.BOLD,14f);

                UIManager.put("gameStartButton.font", customFont);
                UIManager.put("Label.font", customFont);
                UIManager.put("TextField.font", customFont);
                UIManager.put("settingButton.font", customFont);
                UIManager.put("gameExitButton.font", customFont);

                TetrisSignUpScreen signUpScreen = new TetrisSignUpScreen();
                signUpScreen.setVisible(true);
            } catch (IOException | FontFormatException e) {
                e.printStackTrace();
                JOptionPane.showMessageDialog(null, "폰트 로딩 중 오류가 발생했습니다.");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        getContentPane().setBackground(Color.WHITE);

        // 게임 시작 버튼
        JButton gameStartButton = new JButton("Game Start");
        gameStartButton.addActionListener(e -> {
        TetrisModeSelectScreen tetrisModeSelectScreen = new TetrisModeSelectScreen();
        tetrisModeSelectScreen.setLocation(getLocation());
        tetrisModeSelectScreen.setVisible(true);
        dispose();
        });

        // 설정창 진입 버튼
        JButton settingButton = new JButton("Settings");
        settingButton.addActionListener(e -> {
            SettingMenu settingMenu = new SettingMenu();
            settingMenu.setLocation(getLocation());
            settingMenu.setVisible(true);
            dispose();
        });

        // 게임 종료 버튼
        JButton gameExitButton = new JButton("Exit");
        gameExitButton.addActionListener(e -> System.exit(0));

        setTitle("MainMenu");
        setSize(300, 500);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocationRelativeTo(null);

        JPanel buttonPanel = new JPanel(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(25, 5, 25, 5);

        gbc.gridx = 0;
        gbc.gridy = 0;
        buttonPanel.add(gameStartButton, gbc);

        gbc.gridx = 0;
        gbc.gridy = 1;
        buttonPanel.add(settingButton, gbc);

        gbc.gridx = 0;
        gbc.gridy = 2;
        buttonPanel.add(gameExitButton, gbc);

        add(buttonPanel, BorderLayout.CENTER);
    }
}
