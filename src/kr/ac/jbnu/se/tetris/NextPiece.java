package kr.ac.jbnu.se.tetris;

import java.util.Random;

public class NextPiece {
    private Board board;
    private Tetrominoes nextPiece;
    private Shape nextshape;

    private final Random random = new Random();
    public NextPiece() {
        generateNextPiece();
    }

    public void generateNextPiece() {
        int randomIndex;

        do {
            randomIndex = random.nextInt(Tetrominoes.values().length); // NoShape를 피하기 위해
        } while (Tetrominoes.values()[randomIndex] == Tetrominoes.NO_SHAPE || Tetrominoes.values()[randomIndex] == Tetrominoes.ONE_SHAPE ||Tetrominoes.values()[randomIndex] == Tetrominoes.HEAVY_SHAPE);

        nextPiece = Tetrominoes.values()[randomIndex];
        nextshape = new Shape();
        nextshape.setShape(nextPiece);

        System.out.println("Generated next piece: " + nextPiece); // Add this line for debugging
    }

    public Tetrominoes getNextPiece() {

        return nextPiece;
    }
    public Shape getNextshape() {
        return nextshape;
    }


    public int x(int i) {
        if (nextPiece != null && i >= 0 && i < 4) {
            // Replace this with the actual x-coordinate retrieval logic based on your Tetrominoes structure
            return nextshape.getCoordinates()[i][0];
        } else {
            // Handle invalid input or null nextPiece
            return -1;  // For example, returning -1 as an indication of an error
        }
    }


    public int y(int i) {
        if (nextPiece != null && i >= 0 && i < 4) {
            // Replace this with the actual x-coordinate retrieval logic based on your Tetrominoes structure
            return nextshape.getCoordinates()[i][1];
        } else {
            // Handle invalid input or null nextPiece
            return -1;  // For example, returning -1 as an indication of an error
        }
    }
}
