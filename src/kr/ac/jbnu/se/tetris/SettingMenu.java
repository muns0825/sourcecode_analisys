package kr.ac.jbnu.se.tetris;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;

public class SettingMenu extends JFrame {
    static JComboBox bgmCombo;
    static BgmSound curBgm = new BgmSound("sound/Theme1.wav");
    static final String THEME_1 = "<html>Theme 1<br>&lt;Tetris - Bradinsky&gt;</html>";
    static final String THEME_2 = "<html>Theme 2<br>&lt;vvvvvv - Pressure Cooker&gt;</html>";
    static final String THEME_3 = "<html>Theme 3<br>&lt;Kirby - Green Greens&gt;</html>";
    static final String THEME_4 = "<html>Theme 4<br>&lt;LaTale - DotNuri Lv7&gt;</html>";
    static final String THEME_5 = "<html>Theme 5<br>&lt;Pokémon - Red·Green Title&gt;</html>";
    static String curTheme = THEME_1;
    static JComboBox skinCombo;
    static final String SKIN_1 = "<html>Skin 1<br>&lt;Basic&gt;</html>";
    static final String SKIN_2 = "<html>Skin 2<br>&lt;Spring&gt;</html>";
    static final String SKIN_3 = "<html>Skin 3<br>&lt;Summer&gt;</html>";
    static final String SKIN_4 = "<html>Skin 4<br>&lt;Autumn&gt;</html>";
    static final String SKIN_5 = "<html>Skin 5<br>&lt;Winter&gt;</html>";
    static String curSkin = SKIN_1;


    public SettingMenu() {

        try {
            String fontPath = "C:\\TETRIS\\TTRS\\font\\Mabinogi_Classic_TTF.ttf";
            File fontFile = new File(fontPath);

            // 예외 처리 추가
            try {
                Font customFont = Font.createFont(Font.TRUETYPE_FONT, fontFile).deriveFont(Font.BOLD,14f);

                UIManager.put("Button.font", customFont);
                UIManager.put("Label.font", customFont);
                UIManager.put("TextField.font", customFont);
                UIManager.put("PasswordField.font", customFont);

                TetrisSignUpScreen signUpScreen = new TetrisSignUpScreen();
                signUpScreen.setVisible(true);
            } catch (IOException | FontFormatException e) {
                e.printStackTrace();
                JOptionPane.showMessageDialog(null, "폰트 로딩 중 오류가 발생했습니다.");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        getContentPane().setBackground(Color.WHITE);

        // 설정완료 버튼
        JButton doneButton = new JButton("D O N E");
        doneButton.addActionListener(e -> {
            MainMenu mainMenu = new MainMenu();
            mainMenu.setLocation(getLocation());
            mainMenu.setVisible(true);
            dispose();
        });

        setTitle("Settings");
        setSize(300,500);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocationRelativeTo(null);

        Container contentPane = getContentPane();

        JPanel topPanel = new JPanel();
        JPanel middlePanel = new JPanel(new GridBagLayout());
        JPanel bottomPanel = new JPanel();

        JLabel settingLabel = new JLabel("S E T T I N G S");
        JLabel bgmThemeLabel = new JLabel("BGM Theme");
        JLabel blockSkinThemeLabel = new JLabel("<html>BlockSkin<br>&nbsp;&nbsp;Theme</html>");
        JLabel musicVolumeLabel = new JLabel("Music Volume");
        JLabel soundFxVolumeLabel = new JLabel("<html>Sound FX<br>&nbsp;Volume</html>");
        JLabel soundOnOffLabel = new JLabel("Sound on / off");

        String[] bgmList = {THEME_1, THEME_2, THEME_3, THEME_4, THEME_5};
        String[] SkinList = {SKIN_1, SKIN_2, SKIN_3, SKIN_4, SKIN_5};

        //BGM 선택창
        bgmCombo = new JComboBox(bgmList);
        bgmCombo.addActionListener(e -> {
            JComboBox bgmBoxList = (JComboBox) e.getSource();
            String selectedTheme = (String) bgmBoxList.getSelectedItem();
            assert selectedTheme != null;
            if (!selectedTheme.equals(curTheme)) {
                curTheme = selectedTheme;
                switch (curTheme) {

                    case THEME_2 -> {
                        curBgm.BgmStop();
                        curBgm = new BgmSound("sound/theme2.wav");
                        curBgm.BgmStart();
                    }
                    case THEME_3 -> {
                        curBgm.BgmStop();
                        curBgm = new BgmSound("sound/theme3.wav");
                        curBgm.BgmStart();
                    }
                    case THEME_4 -> {
                        curBgm.BgmStop();
                        curBgm = new BgmSound("sound/theme4.wav");
                        curBgm.BgmStart();
                    }
                    case THEME_5 -> {
                        curBgm.BgmStop();
                        curBgm = new BgmSound("sound/theme5.wav");
                        curBgm.BgmStart();
                    }
                    default -> {
                        curBgm.BgmStop();
                        curBgm = new BgmSound("sound/theme1.wav");
                        curBgm.BgmStart();
                    }
                }
            }
        });
        if(curTheme != null){
            bgmCombo.setSelectedItem(curTheme);
        }

        // 블록 스킨 선택창
        skinCombo = new JComboBox(SkinList);
        skinCombo.addActionListener(e -> {
            JComboBox skinBoxList = (JComboBox) e.getSource();
            String selectedSkin = (String) skinBoxList.getSelectedItem();
            assert selectedSkin != null;
            if (!selectedSkin.equals(curSkin)) {
                curSkin = selectedSkin;
            }
        });
        if(curSkin != null){
            skinCombo.setSelectedItem(curSkin);
        }

        topPanel.add(settingLabel);
        bottomPanel.add(doneButton);

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(25, 5, 25, 5);

        gbc.gridx = 0;
        gbc.gridy = 0;
        middlePanel.add(bgmThemeLabel, gbc);

        gbc.gridx = 1;
        gbc.gridy = 0;
        middlePanel.add(bgmCombo, gbc);

        gbc.gridx = 0;
        gbc.gridy = 1;
        middlePanel.add(blockSkinThemeLabel, gbc);

        gbc.gridx = 1;
        gbc.gridy = 1;
        middlePanel.add(skinCombo, gbc);

        gbc.gridx = 0;
        gbc.gridy = 2;
        middlePanel.add(musicVolumeLabel, gbc);

        gbc.gridx = 1;
        gbc.gridy = 2;
        middlePanel.add(new JPanel(), gbc); // 공백 열 추가

        gbc.gridx = 0;
        gbc.gridy = 3;
        middlePanel.add(soundFxVolumeLabel, gbc);

        gbc.gridx = 1;
        gbc.gridy = 3;
        middlePanel.add(new JPanel(), gbc); // 공백 열 추가

        gbc.gridx = 0;
        gbc.gridy = 4;
        middlePanel.add(soundOnOffLabel, gbc);

        gbc.gridx = 1;
        gbc.gridy = 4;
        middlePanel.add(new JPanel(), gbc); // 공백 열 추가

        contentPane.add(topPanel, BorderLayout.NORTH);
        contentPane.add(middlePanel, BorderLayout.CENTER);
        contentPane.add(bottomPanel, BorderLayout.SOUTH);
    }
}
