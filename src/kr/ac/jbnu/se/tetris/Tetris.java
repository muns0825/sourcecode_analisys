package kr.ac.jbnu.se.tetris;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;

public class Tetris extends JFrame {

    final JLabel statusbar; // 게임 상태를 표시하는 레이블

    public Tetris(String modeType, FirebaseScoreManager firebaseScoreManager, FirebaseAuthManager authManager) {

        statusbar = new JLabel(); // 초기 상태바 레이블 설정
        add(statusbar, BorderLayout.SOUTH); // 상태바를 프레임 아래쪽에 추가

        Board board = new Board(this, modeType, firebaseScoreManager); // 수정된 부분
        add(board); // 게임 보드를 프레임에 추가
        board.start(); // 게임 시작

        setSize(400, 800);
        setTitle("Tetris"); // 프레임 제목 설정
        setDefaultCloseOperation(EXIT_ON_CLOSE); // 프레임을 닫을 때 프로그램 종료
        setLocationRelativeTo(null);

    }

    // 상태바 레이블을 반환하는 메서드
    public JLabel getStatusBar() {
        return statusbar;
    }

}
