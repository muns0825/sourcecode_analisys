package kr.ac.jbnu.se.tetris;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class TetrisModeSelectScreen extends JFrame {

    private static final String MODE_NORMAL = "Normal";
    private static final String MODE_TIME_ATTACK = "TimeAttack";

    private transient final FirebaseScoreManager firebaseScoreManager;
    private transient final FirebaseAuthManager authManager;

    public TetrisModeSelectScreen() {

        getContentPane().setBackground(Color.WHITE);

        setTitle("Mode Selection");
        setSize(800, 400);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocationRelativeTo(null);

        firebaseScoreManager = new FirebaseScoreManager(FirebaseAuthManager.getLoggedInUserID());
        authManager = new FirebaseAuthManager();

        JPanel panel = new JPanel(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(5, 5, 5, 5);

        addButton(panel, "TimeAttack Mode", 0, e -> startGame(MODE_TIME_ATTACK));
        addButton(panel, "Normal Mode", 1, e -> startGame(MODE_NORMAL));
        addButton(panel, "Back to Menu", 2, e -> backToMenu());

        add(panel);
    }

    private void addButton(JPanel panel, String buttonText, int gridY, ActionListener actionListener) {
        JButton button = new JButton(buttonText);
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = gridY;
        gbc.gridwidth = 2;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        panel.add(button, gbc);
        button.addActionListener(actionListener);
    }

    private void startGame(String modeType) {
        dispose();
        if (MODE_NORMAL.equals(modeType)) {
            new TetrisNormalModeScreen(modeType, firebaseScoreManager, authManager).setVisible(true);
        } else if (MODE_TIME_ATTACK.equals(modeType)) {
            new TetrisTimeAttackModeScreen(modeType, firebaseScoreManager, authManager).setVisible(true);
        }

    }

    private void backToMenu() {
        dispose();
        MainMenu mainMenu = new MainMenu();
        mainMenu.setVisible(true);
        mainMenu.setLocation(getLocation());
    }

    public static void main(String[] args) {
        /*SwingUtilities.invokeLater(() -> {
            TetrisModeSelectScreen modeSelectScreen = new TetrisModeSelectScreen();
            modeSelectScreen.setVisible(true);
        }); */
    }
}
