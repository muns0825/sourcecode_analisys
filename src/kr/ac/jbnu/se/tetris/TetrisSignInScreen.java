package kr.ac.jbnu.se.tetris;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;

public class  TetrisSignInScreen extends JFrame {
    private final JTextField userIDField;
    private final JPasswordField passwordField;

    public TetrisSignInScreen() {

        // Firebase 초기화
        final FirebaseAuthManager authManager = new FirebaseAuthManager();

        try {
            String fontPath = "C:\\TETRIS\\TTRS\\font\\Mabinogi_Classic_TTF.ttf";
            File fontFile = new File(fontPath);

            // 예외 처리 추가
            try {
                Font customFont = Font.createFont(Font.TRUETYPE_FONT, fontFile).deriveFont(Font.BOLD,14f);

                UIManager.put("Button.font", customFont);
                UIManager.put("Label.font", customFont);
                UIManager.put("TextField.font", customFont);
                UIManager.put("PasswordField.font", customFont);

                TetrisSignUpScreen signUpScreen = new TetrisSignUpScreen();
                signUpScreen.setVisible(true);
            } catch (IOException | FontFormatException e) {
                e.printStackTrace();
                JOptionPane.showMessageDialog(null, "폰트 로딩 중 오류가 발생했습니다.");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        getContentPane().setBackground(Color.WHITE);

        setTitle("Tetris Login");
        setSize(800, 400);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLayout(new GridBagLayout());

        JPanel contentPanel = new JPanel();
        contentPanel.setLayout(new BorderLayout());
        getContentPane().add(contentPanel);

        JPanel inputPanel = new JPanel();
        inputPanel.setLayout(new GridBagLayout());

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(5, 5, 5, 5);

        JLabel userIDLabel = new JLabel("UserID:");
        gbc.gridx = 0;
        gbc.gridy = 0;
        add(userIDLabel, gbc);

        userIDField = new JTextField(20);
        gbc.gridx = 1;
        gbc.gridy = 0;
        add(userIDField, gbc);

        JLabel passwordLabel = new JLabel("Password:");
        gbc.gridx = 0;
        gbc.gridy = 1;
        add(passwordLabel, gbc);

        passwordField = new JPasswordField(20);
        gbc.gridx = 1;
        gbc.gridy = 1;
        add(passwordField, gbc);

        JButton loginButton = new JButton("Login");
        loginButton.setBackground(Color.WHITE);
        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.gridwidth = 2;
        add(loginButton, gbc);

        JButton signUpButton = new JButton("Sign Up");
        signUpButton.setBackground(Color.WHITE);
        gbc.gridx = 0;
        gbc.gridy = 3;
        gbc.gridwidth = 2;
        add(signUpButton, gbc);

        loginButton.addActionListener(e -> {
            String userID = userIDField.getText();
            char[] password = passwordField.getPassword();
            String passwordStr = new String(password);
            // Firebase를 사용하여 사용자 인증 수행
            authManager.signIn(userID, passwordStr, success -> {
                if (success) {
                    SettingMenu.curBgm.BgmStart();
                    System.out.println("User logged in successfully!");
                    // 로그인 성공 후 메인메뉴로 이동
                    MainMenu mainMenu = new MainMenu();
                    mainMenu.setVisible(true);
                    dispose();
                } else {
                    JOptionPane.showMessageDialog(TetrisSignInScreen.this, "로그인 실패: 사용자 정보가 올바르지 않습니다.");
                }
            });
        });

        signUpButton.addActionListener(e -> {
            // 회원가입 화면으로 이동
            TetrisSignUpScreen signUpScreen = new TetrisSignUpScreen();
            signUpScreen.setVisible(true);
            dispose();
        });
        setLocationRelativeTo(null);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            TetrisSignInScreen loginScreen = new TetrisSignInScreen();
            loginScreen.setVisible(true);
        });
    }
}
