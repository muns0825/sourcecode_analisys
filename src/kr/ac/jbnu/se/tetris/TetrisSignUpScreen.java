package kr.ac.jbnu.se.tetris;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;

public class TetrisSignUpScreen extends JFrame {
    private final JTextField userIDField;
    private final JPasswordField passwordField;

    public TetrisSignUpScreen() {


        getContentPane().setBackground(Color.WHITE);

        setLocationRelativeTo(null);

        getContentPane().setBackground(Color.WHITE);

        final FirebaseAuthManager authManager = new FirebaseAuthManager();
        FirebaseManger.init();

        setTitle("Tetris Sign Up");
        setSize(800, 400);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLayout(new GridBagLayout());
        setLocationRelativeTo(null);

        JPanel contentPanel = new JPanel();
        contentPanel.setLayout(new BorderLayout());
        getContentPane().add(contentPanel);

        JPanel inputPanel = new JPanel();
        inputPanel.setLayout(new GridBagLayout());

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(5, 5, 5, 5); // 컴포넌트 간 간격

        JLabel userIDLabel = new JLabel("UserID:");
        gbc.gridx = 0;
        gbc.gridy = 0;
        add(userIDLabel, gbc);

        userIDField = new JTextField(20);
        gbc.gridx = 1;
        gbc.gridy = 0;
        add(userIDField, gbc);

        JLabel passwordLabel = new JLabel("Password:");
        gbc.gridx = 0;
        gbc.gridy = 1;
        add(passwordLabel, gbc);

        passwordField = new JPasswordField(20);
        gbc.gridx = 1;
        gbc.gridy = 1;
        add(passwordField, gbc);

        JButton signUpButton = new JButton("Sign Up");
        signUpButton.setBackground(Color.WHITE);
        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.gridwidth = 2;
        add(signUpButton, gbc);

        signUpButton.addActionListener(e -> {
            String userID = userIDField.getText();
            char[] password = passwordField.getPassword();
            String passwordStr = new String(password);

            authManager.signUp(userID, passwordStr, success -> {
                if (success) {
                    // 회원가입 성공 후 TetrisSignInScreen으로 이동
                    TetrisSignInScreen signInScreen = new TetrisSignInScreen();
                    signInScreen.setVisible(true);
                    dispose();
                } else {
                    JOptionPane.showMessageDialog(TetrisSignUpScreen.this, "회원가입 실패: 이미 존재하는 사용자이거나 입력이 올바르지 않습니다.");
                }
            });
        });

        JButton backButton = new JButton("Back");
        backButton.setBackground(Color.WHITE);
        gbc.gridx = 0;
        gbc.gridy = 3;
        gbc.gridwidth = 2;
        add(backButton, gbc);

        backButton.addActionListener(e -> {
            TetrisSignInScreen tetrisSignInScreen = new TetrisSignInScreen();
            tetrisSignInScreen.setVisible(true);
            dispose();
        });
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            TetrisSignUpScreen signUpScreen = new TetrisSignUpScreen();
            signUpScreen.setVisible(true);
        });
    }
}
