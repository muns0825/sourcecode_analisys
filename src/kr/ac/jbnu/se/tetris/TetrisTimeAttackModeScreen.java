package kr.ac.jbnu.se.tetris;

import javax.swing.*;

public class TetrisTimeAttackModeScreen extends JFrame{
    public TetrisTimeAttackModeScreen(String modeType, FirebaseScoreManager firebaseScoreManager, FirebaseAuthManager authManager) {

        // Tetris 게임 화면을 나타내는 객체
        Tetris tetris = new Tetris(modeType, firebaseScoreManager, authManager); // Tetris 생성자에 모드 타입과 FirebaseScoreManager 전달
        tetris.setVisible(true);
    }
}
