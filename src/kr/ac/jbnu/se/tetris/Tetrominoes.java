package kr.ac.jbnu.se.tetris;
public enum Tetrominoes {
    NO_SHAPE,    // 빈 공간 (블록 없음)
    Z_SHAPE,     // Z 모양 블록
    S_SHAPE,     // S 모양 블록
    I_SHAPE,     // I 모양 블록
    T_SHAPE,     // T 모양 블록
    O_SHAPE,     // O 모양 블록
    L_SHAPE,     // L 모양 블록
    J_SHAPE,     // J 모양 블록
    ONE_SHAPE,    // 1칸짜리 블록
    HEAVY_SHAPE  // 모루블럭
}
